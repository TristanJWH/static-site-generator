---
title: Notion
description: Notion is a note-taking, project management and to do system all-in-one
categories: Applications
thumbnail: img/notion.jpg
number: 1.8
Pub: Mon, 19 Apr 2021 19:41:00 +0100
---
---

# Notion

![My Notion Dashboard](img/notion.jpg)

## What is Notion?

On its website, [Notion](https://www.notion.so/) advertises itself as "One tool for your whole team. Write, plan, and get organized." This is a fairly accurate description because in actuality it is note-taking, project management and to do system all-in-one with collaboration features so that a team can effectively use it. It is designed so that it is easy to customise every detail about the layout so that it can be more effective for your workflow. This means that, the layout that I have above does not have to be how you use it.

Notion uses a system of pages and databases to show and store information. The pages are relatively simple, they operate mostly how any web page would work with the ability to add images, text and links between pages. Though unlike a website, you don't need to know how to code.

Databases are more complicated. They allow you to store data in columns and rows and then to view that data is various ways. For example, my tasks are stored in a database, but I have 3 different ways of looking at my tasks: a calendar view, which shows me when they are due; an actionable task list; and an inbox list for new tasks.

In addition, Notion allows you to embed database views inside of pages and this allows you to create dashboards that contain all the same data that is stored in the databases but show it in a more easily understandable way. This is how I show all of my tasks on the homepage (see above) in a such a way that I can see all the tasks that I need to do and when I need to do them at a glance.

## Positives

The major positive of Notion is the massive amount of customization that it allows you to do. This means that it is one of the very few productivity tools that I feel truly allows me to work in the way that I want to. This is especially true because of the built-in note-taking support, which many other services simply do not have, that allows me to link to various notes I have taken over time.

The next major positive is the LaTeX equations support. I don't write many equations in my notes but when I do, it is so much better to be able to use the full LaTeX syntax for things like fractions so that I don't have to settle for 1/2 instead of $\frac{1}{2}$ or using x instead of $\times$.

## Negatives

Now for the negatives, Notion has a few but by far the largest is the very steep learning curve. I have experience with databases (admitted not very much) and various other task systems but even then I had to look at several other people's systems on YouTube and the [Notion template gallery](https://www.notion.so/Notion-Template-Gallery-181e961aeb5c4ee6915307c0dfd5156d) before I could even begin to create my own system. Even then, after this I still had to spend a few hours configuring and learning how to config the various components of Notion to get it to a point that I am happy with (though I doubt it will stay the same for long).

Moreover, Notion is not an open source project and for some this may pose as privacy risk that is an unacceptable level of risk since nobody can look at or check the code. For my part I don't mind it because the Notion team seem to be confident that they are not stealing your data, with them saying that "TLDR: Notion does not own your data, nor do we sell it to others or use it for advertising. It's your data, period ✌️".

## Conclusion

In conclusion, Notion is a very useful tool and I have now integrated it into my life in a way that I don't think it could ever be replaced by another lesser system despite the potential of it being or becoming a privacy risk. While other projects like Planner or Joplin exist as open source to do and note-taking systems respectively no project (as far as I'm aware) combines the 2 in a way that is even close to Notion and as a result I will be sticking with it for the time being.

