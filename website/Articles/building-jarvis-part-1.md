---
title: Building Jarvis Part 1
description: Announcing a new project: a voice assistant in python
categories: Programming
thumbnail: img/building-jarvis-part-1.jpg
number: 1.2
Pub: Thu, 4 Feb 2021 17:02:48 +0000
---
---

# Building Jarvis Part 1

![Image by Jonas Leupe on Unsplash](img/building-jarvis-part-1.jpg)

## Why:

I like the idea of Cortana. I want a way to control my computer with my voice. While many hate Cortana I believe that if I had full control over the voice assistant in my computer I could get it to a point that it would be a help, not a hindrance.

I also like the idea of talking to an assistant to get the weather and news on my Echo Dot but if that is all it can do then I will use it only once a day.
What I want is a way to control my computer with my voice but also have it be able to talk back and reply to simpler questions.
Some of you may be asking yourselves why I do not just use an open-source assistant like Mycroft AI.
I have looked into it but it seems like it would be a very hacky process to get it to launch applications whereas a personalised solution such as this one can be built to perform anything.

## The Goal:

My goal with this project is to have a fully functioning voice assistant that can perform all of the same tasks that my Echo Dot can already but written by me.

It should be able to perform a basic list of tasks that include:

* Search Wolframaplha (the internet)
* Search Wikipedia
* Tell and show me the weather
* Open applications on my computer
* Go to sleep (stop listening)
* Power off my computer
* Runs on Linux
* Have an easy enough programming interface to help me add any others

So as you can see not an insignificant amount.
However, I have done some preliminary research and it seems we can get other people to fo a lot of the work.
Both WolframAlpha and Wikipedia have APIs and google has both text to speech and speech to text programs that are freely available.
This should make our jobs significantly easier as we just need to stick it all together.

## How I Will Code It:

I will be using the python programming language for this project because it is easy to understand and has lots of support for various websites that I want to interface with.
As previously mentioned I will be using Google's speech to text and text to speech modules.

This may annoy some as one of the main disadvantages of a voice assistant is that they are always listening and sending data to companies like Google but Google has one of if not the best AI as a result of this and I want it to work well.
Besides, you can always change it in your version. 
