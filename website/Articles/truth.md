---
title: The Truth, The Whole Truth and Nothing but The Truth
description: My ambling opinions on the nature of truth and how it affects our lives
categories: Essay
thumbnail: img/truth.jpg
number: 2.0
Pub: Sat, 23 Apr 2022 17:02:48 +0000
---
---



# The Truth, The Whole Truth and Nothing but The Truth

![Photo by [Markus Winkler](https://unsplash.com/@markuswinkler?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/truth?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)](img/truth.jpg)

Truth is a very slippery idea. It is all very well to define it: the extent to which a statement can be considered accurate as a description of reality. However, while it is a word that is easily defined in theory, but near impossible to apply in practice.

## Truth in Science

In science, an idea is considered proven if it passes the peer-review process: checking by two experts in the field. However, this is where we encounter a problem. Despite the idea being proven, the idea is not necessarily true. For instance, Newton's ideas of gravity were [first published](https://plato.stanford.edu/entries/newton-principia/) in 1687 and were considered proven scientific ideas for hundreds of years. However, then Einstein had his ideas of [general relativity](https://youtu.be/XRr1kaXKBsU) and in doing so disproved Newton's ideas.

This leaves us with a dilemma because if we consider Einstein's ideas on gravity to be true, then we must also concede that Newton's ideas were true despite the fact that they have been disproven. Or perhaps we should consider neither to be true; both theories are just that after all: theories. No good scientist would ever claim to know for a fact that a theory is true, simply that it agreed with experimental data.

Not that this particularly helps us, we want certainty in our lives. We want to know that when we drop an object it will definitely fall, and it doesn't seem like science can help us with that.

This is due to a problem in the way that most of us think about science and truth in general. As humans, we want facts and laws that allow us to predict how the world will change if we do certain actions. From an evolutionary stance, this is extremely useful. We need to know that if we go towards that lion, we will be eaten. 

However, the truth doesn't really work like that. In order to create the laws and facts and theories that we call science, a method must be applied. A hypothesis must be created, an experiment must be carried out, and a conclusion must be drawn. And even after all of that, we must constantly retest and refine our conclusions to ensure their validity and accuracy. 

What this means is that science can't really provide us with the truth, but it can provide us with the next best thing. A method to get closer and closer to the truth. Whether a unified theory of everything is even possible is uncertain and another debate, but what we can say with reasonable certainty is that every discovery, every theory gets us a little closer; even if the theory is ultimately proven wrong (well not every discovery but the trend is certainly towards truth). 

Nowhere (or I guess no-when) has this become more apparent than during the [Covid-19 pandemic](https://www.scientificamerican.com/article/what-science-can-and-cannot-do-in-a-time-of-pandemic/). People and governments have looked to science through the pandemic to explain what the hell is going on: the causes, the symptoms, the efficacy of the vaccine etcetera. But people become frustrated and disillusioned when the scientific consensus changes because the illusion of truth, of certainty that people ascribe to science is [shattered](https://www.weforum.org/agenda/2021/11/heres-how-to-repair-the-damage-covid-19-has-done-to-science/). This can lead people down the path of pseudo-science and [quackery](https://dictionary.cambridge.org/dictionary/english/quackery).

As a result of this, I believe that we need a new way of talking and teaching science. Science is not a list of facts found in a text book nor is it a machine that takes an input and provides us with the true answer; it is an evolving process that creates and testes hypotheses. And yes, that means that the scientific consensus is not always correct, but it does mean that over time it will trend towards correctness. And it does mean that the “guess” of science is a lot better than the guess of any one person.

Ultimately, what we want is a big machine that takes in a question and spits out an answer a short while later, similar to “Deep Thought” in Douglas Adam's “A Hitchhiker's Guide to the Galaxy” and so many people like to imagine that there is one in science (or in pseudo-science). But, sadly, this is no quite the case.

Nevertheless, for determining the way that the universe works, science is our best bet, our best way of getting closer to an ultimate truth. It's just that as a method of finding the truth, it is a very slow and imperfect method not, as some would have us believe, a perfect source of knowledge. But, fortunately or not, it is the best system that we have. 

## The News

The news is a very interesting idea in so much as it is an almost impossible task. We demand an accurate, reliable, complete summary of complex events and ideas everyday in vast quantities from all over the world.

Journalists have a very tough job. To maintain the accuracy of their coverage, they must do the kind of research and fact checking that we would expect for full books. And the topics are frequently politically sensitive and, due to the nature of news, the articles they write are often one of the first sources on these issues, making research far far harder.

Moreover, the role of news in a functioning democracy cannot be understated. The role that the news played during incident such as the [Vietnam War](https://www.britannica.com/topic/The-Vietnam-War-and-the-media-2051426) and the [Snowdon leak](https://www.bbc.co.uk/news/world-us-canada-23123964) and the current invasion of Ukraine have showed the world how important a free press truly is to preserving democracy. If we did not have the news, the world would be a far darker place.

Nevertheless, sometimes the news can become a bit of a minefield of inaccuracies. In Merchants of Doubt, Naomi Oreskes and Erik Conway talk about the effect that these inaccuracies had on the unfolding of the climate crises—climate change, the ozone hole, smoking etc.

In the book, they point out the desire of journalists to present a balanced argument, even when the experts have demonstrated one side to be false. This is only worsened by the companies and individuals with vested interests that often perpetuate controversy even if it goes against the expert consensus. This uncertainty leads to nothing happening, as people do not want to sacrifice their quality of life to potentially prevent unknown risks.

> “We take it for granted that great individuals—Gandhi, Kennedy, Martin Luther King—can have great positive impacts on the world. But we are loath to believe the same about negative impacts—unless the individuals are obvious monsters like Hitler or Stalin. But small numbers of people can have large, negative impacts, especially if they are organised, determined, and have access to power.”<br>
<small>—Merchants of Doubt by Naomi Oreskes and Erik Conway</small>

In my opinion, the above quote is the best summary of the entire book. The book continually points out the strategies employed by a small group of people—referred to by the authors as the merchants of doubt—to create uncertainty where little existed, using their qualifications as scientists to validate their viewpoints, to bamboozle journalists. The way in which these strategies are deconstructed in the book is masterful, though what is striking is the lack of opposition to these strategies by the scientific community.

Most of the scientific community seems to have taken the view that arguing against the merchants of doubt is pointless, that it is their role to do research, not to convince people of the truth. I am of the belief that this is fundamentally wrong. The goal of a scientist is to further human knowledge, to me that means the knowledge of all humans, not just those who read journals.

Furthermore, only by educating the masses can the support required to change the world for the better be created. If people did not know about the danger of smoking or $CO_2$ emissions or CFCs, why would they stop? Yet, when people were told about the danger of CFCs, a massive amount of change was made and now it is [predicted that the ozone whole will be gone entirely by 2040](https://earthobservatory.nasa.gov/world-of-change/Ozone).

The truth will be constantly fought by those with motivation to avoid or cast doubt on it. So we must always be vigilant in our defence of it to prevent the pollution of the truth. The fast pace of our lives prevents many of us from keeping up with this duty as much as we should (I would not be surprised if this article contained a litany of errors, for example), but where we can, we should aim to become more knowledgable on a variety of topics. Though, by its nature, the news cannot always be trusted to meet this need.

## Conclusion

The truth is something that we all want. We all want certainty, we all want to be able to trust the facts that we are given, we all want to know what is going on. But unfortunately, an absolute, incorruptible, completely trustworthy truth is impossible to find (unless you join a cult or something). 

To be clear, I'm not saying you shouldn't have faith that something is true: I myself recognise that I am placing faith in science that I do not even close to fully understand when I tell you that climate change is happening. Nevertheless, we must all recognise that faith is not the same as truth, we must constantly question our faith if we are to be sure that we still believe and if we don't: adjust our actions accordingly.

## Further resources
* [Merchants of Doubt by Naomi Oreskes and Erik Conway](https://www.goodreads.com/book/show/7799004-merchants-of-doubt?ac=1&from_search=true&qid=o0Zzi3rGeo&rank=1)
* [Don't Talk to the Police by Regent Law Professor James Duane - Regent University School of Law](https://www.youtube.com/watch?v=d-7o9xYp7eE)
* [The replication crisis won’t be solved with broad brushstrokes](https://www.nature.com/articles/d41586-021-01509-7)