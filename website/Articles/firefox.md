---
title: Why I Use Firefox
description: Firefox is my browser of choice due to it's robust selection of features
categories: Applications
thumbnail: img/firefox1.jpg
number: 1.9
Pub: Mon, 28 Jun 2021 12:21:00 +0100
---
---

# Why I Use Firefox

![Mozilla, Firefox and the Firefox logo are trademarks of the Mozilla Foundation in the U.S. and other countries.
](img/firefox1.jpg)


Nearly all tasks on a computer can be accomplished with a web browser, whether it's word processing in Google Docs or video calls in Microsoft Teams, a lot of what we do on a computer happens through a web browser. This makes it quite important to pick a good one that has all the features that you want.

Personally, I have chosen the [Firefox Web Browser](https://www.mozilla.org/en-GB/firefox/new/) as my web browser due to a combination of defending the open web, customisability and privacy.

## The Open Web

### Market Share

<iframe title="" aria-label="chart" id="datawrapper-chart-WL2t8" src="https://datawrapper.dwcdn.net/WL2t8/4/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="493"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();
</script>

According to [NetMarketShare](https://netmarketshare.com/browser-market-share.aspx?options=%7B%22filter%22%3A%7B%7D%2C%22dateLabel%22%3A%22Custom%22%2C%22attributes%22%3A%22share%22%2C%22group%22%3A%22browser%22%2C%22sort%22%3A%7B%22share%22%3A-1%7D%2C%22id%22%3A%22browsersDesktop%22%2C%22dateInterval%22%3A%22Monthly%22%2C%22dateStart%22%3A%222021-05%22%2C%22dateEnd%22%3A%222021-05%22%2C%22segments%22%3A%22-1000%22%7D), as of May 2021, 65% of people use the Chrome browser. While this is a large percentage it is misleading since many browsers, such as Edge, are based on Chrome, making the statistic of people who use browsers based on Chrome closer to 75%.

But hold up a second. What does this "based on Chrome" actually mean? Well, since the main function of a web browser is to display web pages, a significant part of the code of a browser is part of what is referred to as the web rendering engine. This is the part that turns the website code into an interface with text that you can read and buttons that you can press. However, it is a lot of work to create and maintain a web rendering engine, so many web browsers don't bother and simply use code that someone else has written.

This has resulted in there being surprisingly few web rendering engines given the number of web browsers, we will focus on the 3 main ones. First the Gecko engine, this is the engine that the Firefox browser uses, it was created by Mozilla (the developers of Firefox) or use in the Firefox Browser and their other projects. The next engine that we will focus on is the WebKit engine, this is Apple's engine and so is primarily used in the Safari browser. Finally, Blink, this is the engine used by the Chrome browser and all its various spin-offs such as MS Edge, Opera and Brave.

### Google's Control

This is significant due to the control that this gives Google over web standards because if they don't like a decision that is made about the web then they can do their own thing and take 75% of internet users with them. You may think that there is no precedent for this, but in 2009 Google tried to introduce an alternative to HTTP called SPDY that made some (admittedly needed) changes to the protocol. Moreover, Google has been [caught](https://arstechnica.com/gadgets/2018/12/the-web-now-belongs-to-google-and-that-should-worry-us-all) artificially slowing YouTube performance on competitors' browser like Firefox and Edge, presumably to encourage people to use their browser to gain even more control.

This is in direct opposition to the ideals of the open web since it allows one company, namely Google, to control a large portion of the internet into not only using its own standards but also its own services. Therefore, one of the main reasons why I use Firefox is to help prevent Google from fully taking control of the Internet more than they already have.

## Customisation

The next main reason why I use the Firefox Browser is customisation. Firefox has numerous options that allow you to add features and tweak the interface in any number of ways.

### Extensions

First are extensions. Extensions, for those of you who aren't aware, are small add-ons to browsers. They can be anything from password managers to ad blockers, and often provide minor tweaks to the functionality of the browser.

While Chrome has the Chrome Web Store, Firefox has the [Firefox Add-ons](https://addons.mozilla.org/). You may expect that due to Firefox's very small market share that there would be very few extensions, however, this is not the case. I use loads of extensions and I have never found an extension that's in the Chrome Web Store and is not available on Firefox.

### Appearance

![The Firefox Color extension](img/firefox2.jpg)

Firefox is incredibly customisable. If you want we can change the colour of every single aspect of the interface and get a preview of what it will look like. All of this can be achieved using the [Firefox Color](https://addons.mozilla.org/en-GB/firefox/addon/firefox-color/) extension, which is developed by Mozilla - the developers of Firefox. This means that any theme that you can think of you can create; if you want a bright orange and purple theme then go right ahead.

In addition, if you don't like the default layout of the panels then you can go and change the position of any of them simply by clicking ☰ ᐳ More Tools ᐳ Customise Toolbar. This, combined with other more normal controls such as changing the home screen, allows a very fine level of control over the appearance of a tool that we all use so often.

## Privacy

Privacy is becoming a more and more important issue in society today as people realise the extent to which companies, particularly Big Tech, collect data for use in advertising. Firefox is a leader in this space they make prominent claims on their [privacy page](https://www.mozilla.org/en-GB/firefox/privacy/) on their website as to how "Firefox products are designed to protect your privacy".

It also helps that they are an [open-source](https://opensource.org/osd) project meaning that anyone can look at the [code](https://searchfox.org/mozilla-central/source) and check that Firefox is doing what it says it is regarding data collection and privacy in general. Also, Firefox is backed by the [Mozilla Foundation](https://foundation.mozilla.org/en/who-we-are/) and so Firefox isn't beholden to a company and its needs to make money from ads but a foundation with a [manifesto](https://www.mozilla.org/en-US/about/manifesto/).


![The Firefox privacy settings page](img/firefox3.jpg)

Additionally, Firefox has some of the best privacy settings with their Enhanced Tracking Protection to block many trackers as you go around the internet. This is even more impressive since I have personally never experienced a false positive where a site was broken by the protection. In fact, Mozilla is so confident that they enabled Enhanced Tracking Protection by [default](https://blog.mozilla.org/en/products/firefox/todays-firefox-blocks-third-party-tracking-cookies-and-cryptomining-by-default/) in September 2019. Furthermore, the development of privacy extensions such as [Facebook Container](https://addons.mozilla.org/en-GB/firefox/addon/facebook-container/) which prevents "Facebook from tracking you around the web" by Mozilla, shows their commitment to user privacy.