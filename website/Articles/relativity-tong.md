---
title: 'Einstein, Relativity and Gravity Waves' by Professor David Tong
description: A review of a video lecture about the story of discovery of General Relativity given by David Tong
categories: Video-Review
thumbnail: img/relativity-tong.jpg
number: 2.01
Pub: Thu, 20 Oct 2022 17:02:48 +0000
---
---



# 'Einstein, Relativity and Gravity Waves' by Professor David Tong

![Photo by [Dom Fou](https://unsplash.com/@domlafou) on Unsplash](img/relativity-tong.jpg)


['Einstein, Relativity and Gravity Waves' - Professor David Tong](https://www.youtube.com/watch?v=G8r9Dg5PBRk)

## Key Takeaways

- Einstein’s Theory of General Relativity is the theory of gravity that superseded Newton’s theory of gravity
- The speed limit of the universe is the speed of light and even if we are travelling towards light, we will still observe it to be travelling at the speed of light, never any faster
- Gravity is the curvature of space-time and mass distorts space-time (thus producing the aforementioned curvature)
- Even the brightest minds struggle to understand new science because
it is not a static thing, it is a dynamic process of questioning that
requires you to get things wrong many times before you can get them
right
- Einstein was great because he created little thought experiments
that he stretched to their limits and did the maths with that (e.g. what would happen to the earth’s orbit if the sun exploded)

## My Thoughts

I very much agree with Tong’s major point that science is not static. I believe that science has a kind of duality. On the one hand, it is a set of rules and models and laws that define our view of the universe, but it is also far more than that; it is a process. A set of instructions that give us a framework with which to question the universe through the process of creating and testing hypothesises with the goal of creating models.

I think that the story of Einstein and his Theory of Relativity is a perfect example of this, the way that he struggled and got things wrong shows us that the people we call “smart” are not that way because of what they know or because they are some perfect factory of knowledge that spits out only correct theories; they are smart because of their curiosity and ability to question the universe and listen to what it tells them.

I also found the approach to mathematics very refreshing in this lecture. Many pop-sci lectures refuse to show any mathematics for fear of scaring off the non-expert in the field, but I thought that the way in which Tong touched on it without getting so bogged down that it become incomprehensible made the harder concepts of a speed limit to the universe seem far more manageable than some other explanations that I have seen.

> <em> “If I have seen further [than others], it is by standing on the shoulders of giants”</em>—Isaac Newton (1675)

One issue that I do have with the lecture is that it presents science as a very solitary activity. With the exception of a brief reference to David Hilbert, Einstein is presented as a single, very intelligent person who alone had the drive and intellectual capability to create General Relativity. My issue with this as a way of presenting science is that that cannot be true. Even if a scientist never tells anyone of their discovery, so it never undergoes peer review and is never discussed by any other scientists (which is rarely happens as is), that original scientist would have built on the work of other scientists, perhaps taking a law from Newton or an equation from Archimedes. As such science cannot be a solitary process, it is a process that requires collaboration and I feel that this lecture ignored this somewhat (though I don't doubt that Tong is aware of this).


## Further Reading

- [Scientists make first direct detection of gravitational waves by Jennifer Chu | MIT News Office](https://news.mit.edu/2016/ligo-first-detection-gravitational-waves-0211)
- [Why Gravity is NOT a Force by Veritasium](https://www.youtube.com/watch?v=XRr1kaXKBsU)
- [Biography of David Hilbert by Irving Kaplansky | Britannica](https://www.britannica.com/biography/David-Hilbert)
- [Why No One Has Measured The Speed Of Light by Veritasium](https://www.youtube.com/watch?v=pTn6Ewhb27k)
- [Einstein's Famous Blunder by Sixty Symbols](https://www.youtube.com/watch?v=nJsFsjSWYx0)
- [How An Infinite Hotel Ran Out Of Room by Veritasium](https://www.youtube.com/watch?v=OxGsU8oIWjY)