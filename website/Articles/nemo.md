---
title: Nemo File Manager
description: Nemo is a file manager with many useful features
categories: Applications
thumbnail: img/nemo.jpg
number: 1.6
Pub: Thu, 4 Feb 2021 17:02:48 +0000
---
---

# Nemo File Manager

![The Nemo file manager](img/nemo.jpg)

[Nemo](https://github.com/linuxmint/nemo) is the default file manager for the [Cinnamon](https://github.com/linuxmint/cinnamon) desktop environment and I think this is part of the reason that I like it so much.
Cinnamon (and the [Linux Mint](https://linuxmint.com/) project) was my first experience on Linux and one of the things that I thought was the most amazing things in the world was that I could just hit F3 and split the windows in two.
No more messing around with opening new windows just to drag a file around like I would on Windows just a quick and simple split.

File managers are very simple programs, so it may seem that finding one with the desired list of features should be relatively easy.
However, after trying a wide variety of file managers, I have found that many do not have the split screen capabilities that I am looking for.

So what does Nemo provide?
Well not much specifically it does have some useful features such as the split screen that I am looking for, tabbing and the ability to open a terminal in the current windows.
But to those of you know about the other file managers available know that other file managers such as [Dolphin](https://apps.kde.org/en/dolphin) also have these features.

So why do I prefer Nemo over any other file manager?
Firstly, I like the way that I looks.
While I can't rationalise it, I prefer the circular design of the buttons of Nemo in comparison to the more square buttons of a file manager like dolphin.
But secondly (and more confusingly), I don't like how cluttered the interface of dolphin is by default.
I know that I can customise it, but it just feels like a worse experience to have to completely change the design of a program as basic as my file manager.
On the other hand, Nemo's interface makes perfect sense as soon as you install it; I do a few minor tweaks, but then I'm done in maybe 5 minutes start to finish.

If it seems that my reason are very subjective and not at all merely a comparison of features it is because it is because there is very few, very minor difference between file managers.
The reason that do exist are largely subjective such as the default layout which means that these reason become those that matter when choosing a file manager.

These subjective reason make it all the more important that you choose the right file manager for you.
While I may have chosen Nemo for me, you should explore and try as many file managers as possible (maybe even some terminal only ones) before settling on one that you like.

## Installation

Ubuntu:

```bash
sudo apt install nemo
```

Arch

```bash
sudo apt install nemo
```

Fedora

```bash
sudo dnf install nemo
```
