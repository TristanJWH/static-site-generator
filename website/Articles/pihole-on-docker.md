---
title: Setup Pihole on Docker
description: Pihole is a networking wide adblocking solution
categories: Applications
thumbnail: img/pihole-on-docker.jpg
number: 1.7
Pub: Mon, 9 Apr 2021 19:41:00 +0100
---
---

# Setup Pihole on Docker

![The Pihole Dashboard](img/pihole-on-docker.jpg)


## What is a Pihole


> The Pi-hole® is a DNS sinkhole that protects your devices from unwanted content, without installing any client-side software.

A DNS server is a server that converts urls (or domains such as [tristanhodgson.com](https://tristanhodgson.com) into an IP address that the client computer can connect to get a website or similar from.

What Pihole does that is so special is that it allows to block certain demains from connecting. This means that, Pihole can be set up to block advertising domains so that they don't connect and so that you don't have to see the advert. But if the domain is not an advertising domain then Pihole forwards it to what is called an "Upstream DNS Server" to turn the url into an IP address same as normal.

What's more, with Pihole you never get the annoying messages telling you to disable your adblocker because it seems to the website that the server can't connect for perfectly real reasons (e.g. the server is down).

## Why use Docker

Docker is a way of installing applications on servers in a container. A container is an issolated environment that the application runs in that bundles all of the dependiencies for the app.

Normally docker is billed as a faster way of installing apps due to the inclusion of all dependiencies but in this case though, Pihole has a one line command to install it where docker takes a few lines and requires you to install docker.

The reason that I want to use docker for this guide is that I only have one server and it is already running Nextcloud. Both Nextcloud and Pihole want port 80 by default and docker lets you change the port that a container is running on far more easily than the one line command.

## Installing

1. Install Docker

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker $USER
```

2. Download Docker Run Script

```bash
wget https://raw.githubusercontent.com/pi-hole/docker-pi-hole/master/docker_run.sh pihole_docker.sh
```

3. *Optional:* go to line 12 and change the first 80 to an unused port on your device (I used port 81) ***You only need to do this if you have a program running on port 80*** (nothing runs on port 80 by default)

4. If you are on Ubuntu diable systemd DNS server

```bash
sudo systemctl stop systemd-resolved.service
sudo systemctl disable systemd-resolved.service
```

```bash
sudo nano /etc/resolv.conf
```

There should be a line saying `nameserver 127.0.0.53` change it to `nameserver 1.1.1.1`.

What this all does is disables the DNS server that is built into Ubuntu and then (because if we changed nothing we would have no internet) we have changed the DNS server to [Cloudflare's](https://1.1.1.1/) DNS server.

We had to do this because the Ubuntu DNS server uses the same port as Pihole wants to use and this would prevent Pihole from even installing.

5. Execute the script

```bash
sudo bash pihole_docker.sh
```

6. Change the password

```bash
sudo docker exec -it pihole sudo pihole -a -p
```

It will then prompt you to enter a new admin password.

## Open the Admin Pannel

![The Pihole Admin Pannel](img/pihole-on-docker-1.jpg)

1. Find the IP address of the server

If you are running on a server from home then run the command below

```bash
ip a | grep inet | grep 192.168.[0-9]*.[0-9]*
```

The first part of the output should look similar to `inet 192.168.0.99/24`. The IP address is the first number that has 4 numbers between 0 and 255 seperated by a "." in my case 192.168.0.99.

2. Go to the admin pannel

If you didn't do step 3 then open the url http://<IP>/admin in my case [192.168.0.99/admin](http://192.168.0.99/admin).
    
Or if you did do step 3 then go to http://<IP>:<Port>/admin in my case [192.168.0.99:81/admin](http://192.168.0.99:81/admin).
    
3. Enter the password that you set
    
On the left side there should be a sidebar with a button saying Login. Click it and in the password box that appears and enter the password that you set in step 6.
    
## Change Upstream DNS Server

![The Pihole Admin Pannel](img/pihole-on-docker-2.jpg)

This is optional but because I prefer the 1.1.1.1 DNS to the default Upstream DNS of Google because 1.1.1.1 claims to be more private saying "We won't sell your data, ever." whereas Google is well known for massive data colection of its users. Moreover, 1.1.1.1 claims to be faster than all other DNS servers.
    
1. Go to the setting side pannel
2. Go to DNS in the upper pannel
3. Unclick the boxs for "Google (ECS)"
4. Click the 2 left boxes on for Cloudflare DNS
    
## Connecting to Pihole as a DNS server

For this section I will only be covering how to do this on Linux. If you want to do it on another device then you can Google "Change DNS server on <Your OS>".
    
```bash
sudo nano /etc/resolv.conf
```

There should be a line saying `nameserver <An IP address>` change it to `nameserver <Pihole IP>`.
    
```bash
sudo service network-manager restart
```