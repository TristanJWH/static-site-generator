***THIS PROJECT WAS CREATED FOR MY USE AND MAY NOT WORK FOR YOUR USE CASE***
 
My static site generator that I use to generate my [website](https://www.tristanhodgson.com).
 
# Setup
 
1. Clone the repo - `git clone https://gitlab.com/TristanJWH/static-site-generator.git`
1. Go into the static-site-generator/website directory - `cd static-site-generator/website/`
1. Edit the contents of the template files, home-template.html and article-template.html
1. Edit the style sheet, style.css
1. Go into the article directory - `cd Articles`
1. Change the contents of the ".md" files to have your articles in (add and delete files as necessary)
1. Go to the images directory - `cd ../img`
1. Delete my images and place your own
1. Go the logo directory
1. Replace the transparent.png file with your own logo
1. Go into the blind copy directory - `cd ../../blind_copy`
1. Delete the google2e3fd74d2a8e6e76.html file (this is my Google Search Console file)
1. Go back to the main directory - `cd ../../`
1. Run the build file - `python3 app.py`

# Article Formats
 
My articles mostly use pandoc Markdown syntax (it uses pandoc on the back end) but with a few oddities.
 
## Metadata
 
```
---
title: the_title
description: a_longer_description
categories: a_catagory
thumbnail: img/an_image.jpg
number: a_unique_number
Pub: Thu, 4 Feb 2021 17:02:48 +0000
---
---
```
 
* The category is hard coded as either Application or Programming
* The thumbnail must be in the website/img folder.
* The number must be unique as it controls the order that the articles appear in (both in RSS and the home page)
* The publication date must follow the format above
 
## Contents
 
This follows basic Markdown syntax (code is a work in progress though)
 
Note: the title and thumbnail is not inserted automatically into the contents of the article