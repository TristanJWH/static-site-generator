#!/usr/bin/env python3

import glob
import os
from pprint import pprint


os.system("mkdir -p tmp")
os.system("mkdir -p build")

files_to_be_processed = glob.glob("website/Articles/*.md")


def write_file(path, contents):
    file = open(path, "w")
    file.write(contents)
    file.close()


def get_metadata(propertyNum, metadata):
    metadata = metadata.replace("---", "")
    metadata = metadata.split("\n")
    value = metadata[propertyNum]
    value = value.split(": ")
    value = value[1]
    return value


def md_to_html(file_name, content):
    # Write the content to a temporary file and convert it to html
    file_name += ".md"
    f = open(f"tmp/{file_name}", "w+")
    f.write(content)
    f.close()
    new_file_name = file_name.replace("md", "html")
    os.system(f"pandoc tmp/{file_name} -o tmp/{new_file_name} --mathjax")
    # Open the html file
    pandoc_converted_file = open(f"tmp/{new_file_name}", "r")
    pandoc_converted_html = pandoc_converted_file.read()
    pandoc_converted_file.close()
    return pandoc_converted_html


def html_plus_template(content, title):
    template = open(f"website/article-template.html", "r")
    final_converted_file = template.read()
    template.close()
    # Replacing <content></content> with the content of the article
    final_converted_file = final_converted_file.replace(
        "<content></content>", content)
    final_converted_file = final_converted_file.replace(
        "<title>Blog</title>", f"<title>TH - {title}</title>")
    return final_converted_file


def create_homepage(card):
    # Creating home page
    home_file = open(f"website/home-template.html", "r")
    home_page = home_file.read()
    home_file.close()
    home_page = home_page.replace("<cards></cards>", card)
    return home_page


def generate_rss_item(title, link, description, date):
    return f"<item><title>{title}</title><link>{link}</link><description>{description}</description><guid>{link}</guid><pubDate>{date}</pubDate></item>"


all_articles_list = []
catagory_list_programming = []
catagory_list_applications = []
catagory_list_misc = []
rss_contents = "<rss version=\"2.0\" xmlns:atom=\"http://www.w3.org/2005/Atom\"><channel><title>Tristan Hodgson</title><link>https://www.tristanhodgson.com/rss.xml</link><description>The website for Tristan Hodgson</description><language>en</language><image><title>Tristan Hodgson</title><url>https://www.tristanhodgson.com/img/Logo/transparent.png</url><link>https://www.tristanhodgson.com/rss.xml</link></image><atom:link href=\"https://www.tristanhodgson.com/rss.xml\" rel=\"self\" type=\"application/rss+xml\" />"

for current_file in files_to_be_processed:
    # Open file
    f = open(current_file, "r")
    whole_file = f.read()
    f.close()
    # Split file into content and metadata
    metadata = whole_file.split("---\n---")[0]
    content = whole_file.split("---\n---")[1]
    file_name = current_file.split("website/Articles/")[1].split('.')[0]
    # Format Code blocks
    content = content.replace("```python\n", '<pre><code class="language-python">')
    content = content.replace("```bash\n", '<pre><code class="language-Bash">')
    content = content.replace("```", "</code></pre>")
    # Get metadata values
    title = get_metadata(1, metadata)
    description = get_metadata(2, metadata)
    catagory = get_metadata(3, metadata)
    thumnail = get_metadata(4, metadata)
    catagory = get_metadata(3, metadata)
    number = get_metadata(5, metadata)
    pub = get_metadata(6, metadata)
    # Generate html file
    html_file = md_to_html(file_name, content)
    html_file = html_plus_template(html_file, title)
    # Putting the final_converted_file into a file in the build folder
    write_file(f"build/{file_name}.html", html_file)
    # Generate RSS item
    link = f"https://www.tristanhodgson.com/{file_name}.html"
    rss_item = generate_rss_item(title, link, description, pub)
    rss_contents += rss_item
    # Generate card
    card = f'<a href = "{file_name}.html" style = "text-decoration: none;"><div class = "card mb-4" ><div class = "row no-gutters" ><div class = "col-xl-4" ><img src = "{thumnail}" class = "card-img" / ></div > <div class = "col-md-8" > <div class = "card-body" ><h5 class = "card-title" > {title} </h5 > <p class = "card-text" >{description}</p></div ></div ></div ></div ></a>\n'
    catagory_dictionary = {"card": card,
                           "catagory": catagory, "number": number, "title": title, "thumnail": thumnail, "link": f"{file_name}.html", "description": description}
    all_articles_list.append(catagory_dictionary)
    all_articles_list = sorted(
        all_articles_list, key=lambda k: k['number'], reverse=True)


# Adding the end to the rss file
rss_contents += "</channel></rss>"
# Writing rss file
os.system("touch build/rss.xml")
write_file("build/rss.xml", rss_contents)

# Generate first article Carousel
first_article = all_articles_list[0]
all_articles_list.remove(all_articles_list[0])
first_article_card = f"<div class='card bg-dark text-white'><a href='{first_article['link']}'><img src='{first_article['thumnail']}' class='card-img'><div class='card-img-overlay '><div style='background-color:#343a40a0;'><h5 class='card-title' style='color:#e8e9ea;'>{first_article['title']}</h5><p class='card-text' style='color:#e8e9ea;'>{first_article['description']}</p></div></div></a></div><br><br>"
article_cards = first_article_card

# Creating the main landing page
for article in all_articles_list:
    card = article["card"]
    article_cards += card

home_page = create_homepage(article_cards)
write_file("build/index.html", home_page)

os.system("mkdir -p build/img")
os.system("rm -r build/img > /dev/null")
os.system("cp -r website/img build/img")  # Copy images to the build folder
os.system("cp website/blind_copy/google2e3fd74d2a8e6e76.html build/google2e3fd74d2a8e6e76.html")  # Copy google search dashboard
os.system("cp website/style.css build/style.css")  # Copy stylesheet
os.system("cp website/about.html build/about.html")  # Copy about page
